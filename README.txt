This module takes XML from any URL and transforms it via XSLT.

It can be useful, for example, in the following cases:

- Displaying XML information like weather, currency or the likes that there is
  no point in making into a node.
- Displaying XML informtaion from something that is not a RSS formed XML.
  (Generally the Feeds module and Feeds XPath Parser are better for XML that
  should be turned into nodes. Another module to consider is XML Views)
- When the developer really, really, really likes XSLT. However uncommon that 
  might be.

The user enters a path and some xsl and a block is created that can be used.
