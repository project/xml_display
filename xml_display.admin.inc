<?php
/**
 * @file
 * Administration page callbacks for the xml_display module.
 */


/**
 * Implements hook_form().
 */
function xml_display_edit_form(&$form_state, $op, $xml_display = NULL) {
  if (is_null($xml_display)) {
    $xml_display = array(
      'name' => 'Display Name',
      'path' => 'http://path.to.xml/',
      'elapsed_time' => 0,
      'last_update' => time(),
      'xsl' => '',
      'xml' => NULL,
      'xml_display_id' => NULL,
    );
  }
  $form['xml_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('xml_display'),
    '#suffix' => !is_null($xml_display['xml']) ? "<h3>Preview:</h3>" . $xml_display['xml'] : '',
  );
  $form['xml_display_op'] = array(
    '#type' => 'value',
    '#value' => $op,
  );
  $form['xml_display_id'] = array(
    '#type' => 'value',
    '#value' => $xml_display['xml_display_id'],
  );
  $form['xml_display']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name goes here.'),
    '#default_value' => $xml_display['name'],
  );
  $form['xml_display']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Path goes here.'),
    '#default_value' => $xml_display['path'],
    '#maxlength' => 1024,
  );
  $form['xml_display']['xsl'] = array(
    '#type' => 'textarea',
    '#title' => t('XSL'),
    '#description' => t('XSL goes here.'),
    '#rows' => 20,
    '#default_value' => $xml_display['xsl'],
    '#required' => TRUE,
  );
  $form['xml_display']['xml'] = array(
    '#type' => 'value',
    '#default_value' => $xml_display['xml'],
  );
  $form['xml_display']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['xml_display']['cancel'] = array(
    '#value' => l(t('Cancel'), referer_uri()),
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function xml_display_edit_form_validate($form, &$form_state) {
  if (!preg_match('/http/', $form_state['values']['path'])) {
    form_error($form['path'], t('Path must be a valid http path.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function xml_display_edit_form_submit($form, &$form_state) {
  $xml = xml_display_xml($form_state['values']);
  if ($form_state['values']['xml_display_op'] == 'edit') {
    db_query("UPDATE {xml_display} SET name = '%s', path = '%s', xsl = '%s', xml = '%s' WHERE xml_display_id = %d", $form_state['values']['name'], $form_state['values']['path'], $form_state['values']['xsl'], $xml, $form_state['values']['xml_display_id']);
  }
  elseif ($form_state['values']['xml_display_op'] == 'add') {
    db_query("INSERT INTO {xml_display} (name, path, xsl, xml) VALUES ('%s', '%s', '%s', '%s')", $form_state['values']['name'], $form_state['values']['path'], $form_state['values']['xsl'], $xml);
    $form_state['redirect'] = 'admin/build/xml_display/';
  }
  drupal_set_message(t('Your XML Display has been saved.'));
}

/**
 * List all xml_display.
 */
function xml_display_overview() {
  $header = array(t('Name'), t('Path'), t(''));
  $rows = array();

  $result = db_query('SELECT name, path, xml_display_id FROM {xml_display} ORDER BY xml_display_id');
  while ($res = db_fetch_array($result)) {
    $xml_display[] = $res;
  }

  if (is_array($xml_display)) {
    foreach ($xml_display as $val) {
      $row = array();
      $row[] = l($val['name'], 'admin/build/xml_display/edit/' . $val['xml_display_id']);
      $row[] = t($val['path']);
      $links = array();
      $links[] = l(t('Edit'), 'admin/build/xml_display/edit/' . $val['xml_display_id']);
      $links[] = l(t('Delete'), 'admin/build/xml_display/delete/' . $val['xml_display_id']);
      $row[] = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
      $rows[] = $row;
    }
  }
  $output = theme('table', $header, $rows);
  return $output;
}


/**
 * Xml_display delete page.
 */
function xml_display_admin_delete(&$form_state, $xml_display) {
  $form['xml_display'] = array(
    '#type' => 'value',
    '#value' => $xml_display,
  );
  return confirm_form($form, t('Are you sure you want to delete %xml_display?', array('%xml_display' => $xml_display['name'])), 'admin/build/xml_display', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process xml_display delete form submission.
 */
function xml_display_admin_delete_submit($form, &$form_state) {
  $xml_display = $form_state['values']['xml_display'];
  db_query("DELETE FROM {xml_display} WHERE xml_display_id = %d", $xml_display['xml_display_id']);
  drupal_set_message(t('xml_display %xml_display has been deleted.', array('%xml_display' => $xml_display['name'])));
  $form_state['redirect'] = 'admin/build/xml_display';
  return;
}
